require 'nokogiri'
require 'open-uri'
require 'byebug'
require_relative 'post.rb'
require_relative 'comment.rb'

url = ARGV[0]
page = Nokogiri::HTML(open(url))
title = page.search('.title').map { |link| link.inner_text }
points = page.search('.subtext > span:first-child').map { |span| span.inner_text}
item_id = page.search('.subtext > a:nth-child(3)').map { |link| link['href'] }

post = Post.new(title, url, points, item_id)
puts post.to_s

user_name = page.search('.comhead > a:first-child').map { |user| user.text }
text = page.search('.default > .comment > .c00').map { |comment| comment.inner_text }

comment = Comment.new(user_name, text)
puts comment.to_s
