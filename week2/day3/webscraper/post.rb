class Post

  attr_accessor :title, :url, :points, :item_id

  def initialize(title, url, points, item_id)
    @title = title
    @url = url
    @points = points
    @item_id = item_id
    @comments = []
  end

  def to_s
    "Title: #{@title}, Points: #{@points}, Item ID: #{@item_id}"
  end

  def comments
    @comments
  end

  def add_comment(comment)
    @comments << comment
  end

end
