class Comment

  attr_accessor :user_name, :text

  def initialize(user_name, text)
    @user_name = user_name
    @text = text
  end

  def to_s
    "User: #{@user_name} Comment: #{@text}"
  end

end
