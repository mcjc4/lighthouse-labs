require_relative 'spec_helper'

describe Unit do
  
  before :each do
    @unit = Unit.new(100, 10)
    @unit_dead = Unit.new(0, 80)
  end

  describe "#attack!" do
    it "alive units can't attack dead units" do
      expect(@unit_dead.dead?).to be_truthy
      expect(@unit.attack!(@unit_dead)).to be_falsey
    end

    it "dead units can't attack" do
      expect(@unit_dead.attack!(@unit)).to be_falsey
    end
  end
end