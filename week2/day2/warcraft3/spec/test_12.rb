require_relative 'spec_helper'

describe Unit do
  
  before :each do
    @unit = Unit.new(0, 0)
  end

  describe "#dead?" do
    it "health_points reaches 0 or lower" do
      expect(@unit.dead?).to be_truthy
    end
  end
end