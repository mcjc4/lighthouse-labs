require_relative 'spec_helper'

describe Barracks do
  
  before :each do
    @barracks = Barracks.new
  end

  describe "#health_points" do
    it "Barracks should have 500 health points" do
      expect(@barracks.health_points).to eq(500)
    end
  end
  
  describe "#damage" do
    it "should reduce the barracks health_points by the attack_power specified" do
      @barracks.damage(5)
      expect(@barracks.health_points).to eq(495)
    end
  end
end

describe Footman do

  before :each do
    @footman = Footman.new
    @barracks = Barracks.new
  end

  describe "#attack_barracks" do
    it "atack power is halved when attacking barracks" do
      barracks = Barracks.new
      expect(barracks).to receive(:damage).with(5)
      @footman.attack_barracks(barracks)
    end
  end
end