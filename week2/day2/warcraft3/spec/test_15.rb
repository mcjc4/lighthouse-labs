require_relative 'spec_helper'

describe SiegeEngines do
  
  before :each do
    @siege_engines = SiegeEngines.new
    @barracks = Barracks.new
    @peasant = Peasant.new
    @footman = Footman.new
  end

  it "has and knows its HP, which is 400" do
    expect(@siege_engines.health_points).to eq(400)
  end

  it "has and know its AP, which is 50" do
    expect(@siege_engines.attack_power).to eq(50)
  end

  describe "#attack_barracks" do
    it "double damage on barracks" do
      expect(@barracks).to receive(:damage).with(100)
      @siege_engines.attack_barracks(@barracks)
    end
  end

  describe "#damage" do
    it "should reduce the barracks health_points by the attack_power specified" do
      @barracks.damage(100)
      expect(@barracks.health_points).to eq(400)
    end
  end

  describe "#attack!" do
    it "should not be able to attack footman or peasant" do
      expect(@siege_engines.attack!(@footman)).to be_falsey
      expect(@siege_engines.attack!(@peasant)).to be_falsey
    end
    it "can attack another siege engine" do
      expect(@siege_engines.attack!(@siege_engines)).to be_truthy
    end
  end
end

describe Barracks do 

  before :each do
    @barracks = Barracks.new
  end

  describe "#can_train_siege_engines?" do
    it "returns true if there's enough resources to create a siege engine" do
      expect(@barracks.can_train_siege_engines?).to be_truthy
    end
  end

  describe "#train_siege_engines" do
    it "trains a siege engine if can_train_siege_engines? returns true" do
      expect(@barracks).to receive(:can_train_siege_engines?).and_return(true)
      expect(@barracks.train_siege_engines).to be_a(SiegeEngines)
    end
  end
end