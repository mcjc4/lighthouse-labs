# http://classic.battle.net/war3/human/units/footman.shtml

class Footman < Unit

  attr_reader :health_points, :attack_power

  def initialize
    @health_points = 60
    @attack_power = 10
  end

  def attack!(enemy)
    enemy.damage(@attack_power)
  end

  def damage(hit)
    @health_points -= hit
  end

  def attack_barracks(barracks)
    barracks.damage(@attack_power / 2)
  end

end
