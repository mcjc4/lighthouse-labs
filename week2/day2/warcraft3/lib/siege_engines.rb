class SiegeEngines < Unit

  attr_reader :health_points, :attack_power

  def initialize
    @health_points = 400
    @attack_power = 50
  end

  def attack_barracks(barracks)
    barracks.damage(@attack_power * 2)
  end

  def damage(hit)
    @health_points -= hit
  end

  def attack!(enemy)
    if enemy.is_a?(Footman) || enemy.is_a?(Peasant)
      false
    elsif enemy.is_a?(SiegeEngines)
      true
    end
  end
end