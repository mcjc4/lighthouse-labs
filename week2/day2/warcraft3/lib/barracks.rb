class Barracks < Unit

  attr_reader :gold, :food, :health_points, :lumber

  def initialize
    super(500, 0)
    @gold = 1000
    @food = 80
    @lumber = 500
  end

  def can_train_footman?
    food >= 2 && gold >= 135 ? true : false
  end

  def train_footman
    if can_train_footman? == true
      @gold -= 135
      @food -= 2

      footman = Footman.new
    else
      nil
    end
  end

  def can_train_peasant?
    food >= 5 && gold >= 90 ? true : false
  end

  def train_peasant
    if can_train_peasant? == true
      @gold -= 90
      @food -= 5

      peasant = Peasant.new
    else
      nil
    end
  end

  def can_train_siege_engines?
    food >= 3 && gold >= 200 && lumber >= 60 ? true : false
  end

  def train_siege_engines 
    if can_train_siege_engines? == true
      @gold -= 200
      @food -= 3
      @lumber -=60

      siege_engines = SiegeEngines.new
    else
      nil
    end
  end

end
