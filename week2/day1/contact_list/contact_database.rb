## TODO: Implement CSV reading/writing
require 'csv'
require 'highline/import'
require 'byebug'

class ContactDatabase

  def self.new_contact(name, email)
    new_contact = []

    CSV.foreach("contacts.csv") do |contact|
      if contact[1] == email || contact[1] == name 
        puts "Already exists"
        exit 
      end
    end

    user_phone_num = ask("Do you have a phone number you want to add? Yes/No").downcase
    @phones = {}

    while user_phone_num == "yes"
      type_of_num = ask("What type phone is this?").downcase
      phone_num = ask("What is your phone number?")
      @phones[type_of_num] = phone_num

      user_phone_num = ask("Do you want to add another phone number? Yes/No").downcase
    end

    CSV.open("contacts.csv", "a") do |new_contact|
      id = CSV.open("contacts.csv").readlines.length
      new_contact << [id, name, email, @phones]
    end
  end

end