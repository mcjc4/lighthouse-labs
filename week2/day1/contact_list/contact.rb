class Contact
 
  attr_accessor :name, :email

  def initialize(name, email)
    @name = name
    @email = email
  end
 
  def to_s
    # TODO: return string representation of Contact
  end
 
  ## Class Methods
  class << self
    def create(name, email)
      puts ContactDatabase.new_contact(name, email)
    end
 
    def find(term)
      found_contact = []

      CSV.foreach("contacts.csv") do |contact|
        if contact[1] == term || contact[2] == term
          found_contact << contact.join(" ")
        end
      end
      found_contact
    end
 
    def all
      all_contacts = []

      CSV.foreach("contacts.csv") do |row|
        all_contacts << row.join(" ")
      end
      all_contacts
    end
    
    def show(id)
      show_contacts = []

      CSV.foreach("contacts.csv") do |contact|
        if contact[0].to_i == id
          show_contacts << contact.join(" ")
        end
      end
      show_contacts
    end
  end
 
end