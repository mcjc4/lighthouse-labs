require "highline/import"
require "byebug"
require_relative 'contact'
require_relative 'contact_database'

# TODO: Implement command line interaction
# This should be the only file where you use puts and gets

input = ARGV.first

case input 
  when "help"
  puts "Here is a list of available commands:\n
        new  - Create a new contact\n
        list - List all contacts\n
        show - Show a contact\n
        find - Find a contact"

  when "new"
    name  = ask("Enter your full name.")
    email = ask ("Enter your email")
    Contact.create(name, email)

  when "list"
    puts Contact.all

  when "show"
    puts Contact.show(ARGV[1].to_i)

  when "find"
    puts Contact.find(ARGV[1].downcase)
end