require_relative "animal"
require_relative "primate"
require_relative "chimpanzee"
require_relative "amphibian"
require_relative "frog"
require_relative "mammal"
require_relative "bat"
require_relative "parrot"
require_relative "module"

george = Chimpanzee.new(2,2,"George")
captn = Parrot.new(2,2,"Capt'N")
hoppy = Frog.new(2,2,"Hoppy")
dracula = Bat.new(2,2,"Dracula")

captn.fly