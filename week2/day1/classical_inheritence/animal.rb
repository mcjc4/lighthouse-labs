class Animal

  attr_accessor :num_legs, :num_arms, :name

  def initialize(num_legs, num_arms, name)
    @num_legs = num_legs
    @num_arms = num_arms
    @name = name
  end

end