$(document).ready(function() {

  $("#showAll").click(function() {
    $("#output").empty();

    $.ajax({
      url: "/users",
      type: "get",
      dataType: "json",
    }).done(function(datas) {
      for(data in datas){
        var para = document.createElement("p");
        var email = document.createTextNode(datas[data]["email"]);
        var name = document.createTextNode(datas[data]["name"]);
        var number = document.createTextNode(datas[data]["number"]);
        var view = document.createElement("BUTTON");
        var text = document.createTextNode("View");

        view.appendChild(text);
        view.setAttribute("id", "view");
        para.appendChild(email);
        para.appendChild(name);
        para.appendChild(number);
        para.appendChild(view);

        var element = document.getElementById("output");

        element.appendChild(para);
      }
    });
  });

  $("#createNew").click(function(){
    $("#output").empty();

    var form = document.createElement("form");
    form.setAttribute('method',"post");
    form.setAttribute('remote',"true")

    var email = document.createElement("input");
    email.type = "text";
    email.name = "email";
    email.id = "email";
    email.value = "Enter Email Here";

    var name = document.createElement("input");
    name.type = "text";
    name.name = "name";
    name.id = "name";
    name.value = "Enter Name Here";

    var number = document.createElement("input");
    number.type = "text";
    number.name = "number";
    number.id = "number";
    number.value = "Enter Number Here";

    var submit = document.createElement("input");
    submit.type = "submit";
    submit.value = "Submit";
    submit.id = "submitNew";

    form.appendChild(email);
    form.appendChild(name);
    form.appendChild(number);
    form.appendChild(submit);

    $("#output").append(form);

    $("form").submit(function(event){
      event.preventDefault();
      // var user = $('form');
      // debugger;

      var user = {
        name: number.value,
        email: email.value,
        number: number.value
      }

      $.ajax({
        url: "/users",
        method: "POST",
        data: {
          user: user
        },
        dataType: "json"
      }).done(function(){
        alert("Successfully added new contact");
      });
    });
  });

});
