class UsersController < ApplicationController

  def index
    render json: User.all
  end

  def new
    @user = User.new
  end

  def create
    @user = User.create!(user_params)

    respond_to do |format|
      format.json { render json: @index }
    end
  end

  protected

  def user_params
    params.require(:user).permit(
      :name,
      :email,
      :number
    )
  end

end
