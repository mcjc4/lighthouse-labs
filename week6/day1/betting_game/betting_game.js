var money = +$("#money").text();

$("#start").click(function(event) {
  genNum();
});

function genNum() {
  var randomNum = Math.floor((Math.random() * 10) + 1);

  if(money > 0) {
    compareNum(randomNum);
  } else {
    alert("Not enough money to play!");
  }
}

function compareNum(randomNum) {
  var playerChoice = $("#playerChoice").val();

  if(randomNum == playerChoice) {
    money += +($("#playerBet").val() * 2);
  } else {
    money -= +$("#playerBet").val();
  }

  $("#money").text(money);
};


// var startingMoney = 100;
//
// function regulationBet() {
//   var playerBet = prompt("How much are you going to bet? (Please enter a number between 5 - 10)");
//
//   while(playerBet < 5 || playerBet > 10) {
//     alert("You not inside betting range");
//     var playerBet = prompt("How much are you going to bet? (Please enter a number between 5 - 10)");
//   }
//
//   alert("You bet $" + playerBet);
//     letsPlay(playerBet);
// }
//
// function letsPlay(playerBet) {
//   while(startingMoney > 0) {
//     var randomNum = Math.floor((Math.random() * 10) + 1);
//     var playerChoice = prompt("Pick a number from 1 - 10");
//
//     if(playerChoice == randomNum) {
//       startingMoney += (playerBet * 2);
//       alert("You won the bet! You have $" + startingMoney);
//     } else {
//       startingMoney -= playerBet;
//       alert("You lost the bet! You have $" + startingMoney);
//     }
//
//     regulationBet();
//   }
//
//   alert("Game Over!");
// }
//
// regulationBet();
