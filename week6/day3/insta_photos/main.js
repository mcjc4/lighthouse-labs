$(document).ready(function(){
  const INSTA_API = "https://api.instagram.com/v1/tags/lighthouse/media/recent?client_id=fe21c6af72624e4d9784c95cd0386e80";

  var searchConfiguration = {
		dataType: "jsonp",
    url: INSTA_API
	};

  function getImage(){
    var promise = $.ajax(searchConfiguration);

    promise.then(function(results){
      for(result in results.data){
        var image = results.data[result].images.standard_resolution.url;
        var caption = results.data[result].caption.text;
        var likeCount = results.data[result].likes.count;
        var commentCount = results.data[result].comments.count;
        var instaLink = results.data[result].link;

        $("#photo").fadeOut(700, function(){
          $(this).attr("src", ""+image);
          $(this).fadeIn(700);
        });
        $("#caption").html(caption);
        $("#likeCount").text(likeCount);
        $("#commentCount").text(commentCount);
        $("#instaLink").attr("href", ""+instaLink);
      };
    });
  };

  setInterval(getImage, 10000);

  getImage();
});
