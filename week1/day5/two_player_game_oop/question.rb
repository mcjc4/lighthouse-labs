require "byebug"

class Question

  attr_accessor :num1, :num2, :answer
  attr_reader :chosen_operator

  def initialize
    @num1             = rand(20)
    @num2             = rand(20)
    @operator         = ['+', '-', '*']
    @chosen_operator  = @operator.sample
    @answer           = @num1.send(@chosen_operator, @num2)
  end

end