require_relative "player"
require_relative "question"
require "highline/import"
require "colorize"
require "byebug"

class InvalidGuessError < StandardError
end

class NameError < StandardError
end

begin
  player1 = ask("Player 1, what's your name?")
  raise NameError, "Name can't be empty." if player1.length < 1
  player2 = ask("Player 2, what's your name?")
  raise NameError, "Name can't be empty." if player2.length < 1
end

@player1  = Player.new(player1, 1)
@player2  = Player.new(player2, 2)


@players  = [@player1, @player2]
@turn     = 0

def start_game
  @question = Question.new
  current_player  = switch_player
  begin
    player_answer = ask("What is #{@question.num1} #{@question.chosen_operator} #{@question.num2}?")
    raise InvalidGuessError, "Answer needs to be an integer." unless player_answer.match(/\d/)
  end

  player_answer = player_answer.to_i

  if player_answer == @question.answer
    current_player.score
  else
    current_player.lose_lives
  end

  puts "It's your turn now, #{current_player.name}."
end

def switch_player
  if @turn == 0
    @turn   = 1
  else
    @turn   = 0
  end
  @players[@turn]
end

play = "yes"

while play == "yes"

  while ((@player1.lives > 0) && (@player2.lives > 0))
    start_game
  end

  restart = ask("\nGame is over! Do you want to play again? Yes / No").downcase

  if play = restart
    @player1.reset
    @player2.reset
  else
    exit
  end

  puts "Bye!".colorize(:red)
end