require "byebug"
require "colorize"

class Player

  attr_accessor :name, :lives, :score

  def initialize(name, id)
    @name   = name
    @lives  = 3
    @score  = 0
  end

  def score
    @score += 1
    puts "Correct! You scored a point! You have #{@score} points!".colorize(:green)
  end

  def lose_lives
    @lives -= 1
    puts "Wrong! You lost a live! You have #{@lives} lives left!".colorize(:red)
  end

  def reset
    self.lives = 3 
    self.score = 0 
  end

end