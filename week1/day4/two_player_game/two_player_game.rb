require "highline/import"
require "byebug"
require "colorize"

@player1_lives        = 3
@player2_lives        = 3
@current_player       = 1

def start_round
  @num1 = rand(1..20) 
  @num2 = rand(1..20)

  game_logic
end

def game_logic
  players_lives
  @answer = @num1 + @num2

  user_answer = ask("What is #{@num1} + #{@num2}?").to_i

  if user_answer == @answer
    puts "Correct!".colorize(:green)
  
  else

    puts "Wrong!".colorize(:red)

    if @current_player == 1
      @player1_lives -= 1
    else
      @player2_lives -= 1
    end

  end

  player_switch
end

def player_switch
  if @current_player == 1
    @current_player = 2
  else
    @current_player = 1
  end
end

def players_lives
  if @current_player == 2
    puts "It's now Player #{@current_player}'s turn.\nYou have #{player1_lives} lives left."
  else
    puts "It's now Player #{@current_player}'s turn.\nYou have #{player2_lives} lives left."
  end
end

loop do

  start_round

  break unless ((@player1_lives > 0) && (@player2_lives > 0))
end

puts "Game Over"