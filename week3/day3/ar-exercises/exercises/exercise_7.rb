require 'highline/import'
require_relative '../setup'
# require_relative './exercise_1'
# require_relative './exercise_2'
# require_relative './exercise_3'
# require_relative './exercise_4'
# require_relative './exercise_5'
# require_relative './exercise_6'

puts "Exercise 7"
puts "----------"

# Your code goes here ...

store_name = ask("What is your store name?")
new_store = Store.create(
  name: "#{store_name}",
  annual_revenue: 0,
  # mens_apparel: false,
  # womens_apparel: false,
)
puts new_store.errors.full_messages

# Store.create!(
#   name: 'Test',
#   annual_revenue: 300000,
#   mens_apparel: false,
#   womens_apparel: false,
# )
