class Store < ActiveRecord::Base
  has_many :employees

  validates :name, presence: true, length: { minimum: 3 }
  validates :annual_revenue, numericality: { greater_than: 0 }
  validates :mens_apparel, presence: true
  validates :womens_apparel, presence: true

  validate :must_have_mens_or_womens_apparel

  def must_have_mens_or_womens_apparel
    if mens_apparel == false && womens_apparel == false
      errors.add(:mens_apparel, "must have mens apparel or womens apparel")
      errors.add(:womens_apparel, "must have womens apparel or mens apparel")
    end
  end
end
