require 'highline/import'
require_relative 'setup'

input = ARGV.first

case input
  when "help"
  puts "Here is a list of available commands:\n
        new     - Create a new contact\n
        find    - Find a contact by ID/Name/Email\n
        delete  - Delete a contact"

  when "new"
    name  = ask("Enter your name")
    email = ask ("Enter your email")
    p ContactDatabase.create(name, email)

  when "find"
    term = ask("Enter ID/Name/Email")
    p ContactDatabase.find(term)

  when "delete"
    term = ask("Enter ID/Name/Email that you want to delete")
    p ContactDatabase.destroy(term)
end
