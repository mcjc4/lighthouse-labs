require 'highline/import'
require_relative 'setup'

class ContactDatabase

  def self.create(name, email)

    Contact.create!(
      name: name,
      email: email,
    )
  end

  def self.find(term)
    if term.match(/\d+/)
      Contact.find(term)
    elsif term.match(/[\w]+([\s]+[\w]+){1}+$/)
      Contact.find_by_name(term)
    elsif term.match(/\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i)
      Contact.find_by_email(term)
    end
  end

  def self.destroy(term)
    if term.match(/\d+/)
      Contact.delete(term)
    elsif term.match(/[\w]+([\s]+[\w]+){1}+$/)
      Contact.destroy_all(name: term)
    elsif term.match(/\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i)
      Contact.destroy_all(email: term)
    end
  end
end
