require 'byebug'

class Contact

  attr_accessor :firstname, :lastname, :email

  attr_reader :id

  def initialize(firstname, lastname, email, id = nil)
    @firstname = firstname
    @lastname = lastname
    @email = email
  end

  class << self

    def connection
      connection = PG.connect(
        host: 'localhost',
        dbname: 'contacts',
        user: 'development',
        password: 'development'
      )
    end

    def create(firstname, lastname, email)
      ContactDatabase.new_contact(firstname, lastname, email)
    end

    def find(term)

    end

    def all

    end

    def show(id)

    end
  end

  def save
    sql = "INSERT INTO contacts (firstname, lastname, email) VALUES ($1, $2, $3);"
    Contact.connection.exec_params(sql, [@firstname, @lastname, @email])
    true
  rescue PG::Error
    false
  end

end
