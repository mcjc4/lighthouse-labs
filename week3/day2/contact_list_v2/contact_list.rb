require 'highline/import'
require 'pg'
require_relative 'contact'
require_relative 'contact_database'

input = ARGV.first

case input
  when "help"
  puts "Here is a list of available commands:\n
        new     - Create a new contact\n
        delete  - Delete a contact\n
        list    - List all contacts\n
        show    - Show a contact\n
        find    - Find a contact"

  when "new"
    firstname  = ask("Enter your first name")
    lastname = ask("Enter your last name")
    email = ask ("Enter your email")
    puts Contact.create(firstname, lastname, email)

  when "list"
    Contact.destroy

  when "list"
    Contact.all

  when "show"
    Contact.show(ARGV[1].to_i)

  when "find"
    Contact.find(ARGV[1].downcase)
end
