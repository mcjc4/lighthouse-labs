class TeachersImporter

  def initialize
  end

  def import
    Teacher.create(
    name: 'Gary Stark',
    email: 'gary@mail.com',
    address: '123 Street St',
    phone: '123-123-1234'
    )

  Teacher.create(
    name: 'Bob Stack',
    email: 'bob@mail.com',
    address: '234 Street St',
    phone: '123-123-1235'
    )

  Teacher.create(
    name: 'John Appleton',
    email: 'john@mail.com',
    address: '345 Street St',
    phone: '123-123-1236'
    )

  Teacher.create(
    name: 'Max Prowers',
    email: 'max@mail.com',
    address: '456 Street St',
    phone: '123-123-1237'
    )

  Teacher.create(
    name: 'Andy Goldman',
    email: 'andy@mail.com',
    address: '567 Street St',
    phone: '123-123-1238'
    )

  Teacher.create(
    name: 'Jack Hamilton',
    email: 'jack@mail.com',
    address: '678 Street St',
    phone: '123-123-1239'
    )

  Teacher.create(
    name: 'Moses Saraha',
    email: 'moses@mail.com',
    address: '789 Street St',
    phone: '123-123-1240'
    )

  Teacher.create(
    name: 'Brian Repel',
    email: 'brian@mail.com',
    address: '890 Street St',
    phone: '123-123-1241'
    )

  Teacher.create(
    name: 'Guy Best',
    email: 'guy@mail.com',
    address: '901 Street St',
    phone: '123-123-1242'
    )
  end

end
