class Teacher < ActiveRecord::Base

  has_many :students

  validates :email,
            :name,
            presence: true,
            uniqueness: true

end
