class CreateTeachers < ActiveRecord::Migration

  def change
    create_table :teachers do |t|
      t.column :name, :string
      t.column :email, :string
      t.column :address, :string
      t.column :phone, :string
      t.timestamps null: false
    end
  end

end
