# Exercise 1

SELECT e.isbn
FROM editions AS e
JOIN publishers AS p
ON e.publisher_id = p.id
WHERE p.name = 'Random House';

# Exercise 2

SELECT e.isbn, b.title
FROM editions AS e
JOIN books AS b
ON b.id = e.book_id
JOIN publishers AS p
ON e.publisher_id = p.id
WHERE p.name = 'Random House';

# Exercise 3

SELECT e.isbn, b.title, s.stock, s.retail
FROM editions AS e
JOIN books AS b
ON b.id = e.book_id
JOIN publishers AS p
ON e.publisher_id = p.id
JOIN stock AS s
ON s.isbn = e.isbn
WHERE p.name = 'Random House';

# Exercise 4

SELECT e.isbn, b.title, s.stock, s.retail
FROM editions AS e
JOIN books AS b
ON b.id = e.book_id
JOIN publishers AS p
ON e.publisher_id = p.id
JOIN stock AS s
ON s.isbn = e.isbn
WHERE p.name = 'Random House'
AND s.stock > 0;

# Exercise 5

SELECT e.type,
CASE e.type
  WHEN 'h' THEN 'hardback'
  ELSE 'paperback'
END AS type_of_cover
FROM editions AS e;

# Exercise 6

SELECT b.title, e.publication
FROM books AS b
LEFT JOIN editions AS e
ON e.book_id = b.id;

# Exercise 7

SELECT b.title, count(s.stock) AS stock
FROM books AS b
JOIN editions AS e
ON e.book_id = b.id
JOIN stock AS s
ON s.isbn = e.isbn
GROUP BY b.title;

# Exercise 8

SELECT avg(cost) AS average_cost, avg(retail) AS average_retail, avg(retail - cost) AS average_profit
FROM stock;

# Exercise 9

SELECT e.book_id, sum(s.stock) AS stock
FROM editions AS e
JOIN stock AS s
ON e.isbn = s.isbn
GROUP BY e.book_id
ORDER BY stock DESC
LIMIT 1;

# Exercise 10

SELECT a.id, concat(a.first_name, ' ',a.last_name) AS full_name, count(b.id) AS number_of_books
FROM authors AS a
JOIN books AS b
ON b.author_id = a.id
GROUP BY a.id;

# Exercise 11

SELECT a.id, concat(a.first_name, ' ',a.last_name) AS full_name, count(b.id) AS number_of_books
FROM authors AS a
JOIN books AS b
ON b.author_id = a.id
GROUP BY a.id
ORDER BY number_of_books DESC;

# Exercise 12

SELECT b.title
FROM books AS b
JOIN editions AS e
ON b.id = e.book_id
GROUP BY b.title
HAVING array_agg(e.type) @> '{"p", "h"}';

# Exercise 13

SELECT p.name, round(avg(s.retail), 1), count(e.edition)
FROM publishers AS p
JOIN editions AS e
ON e.publisher_id = p.id
JOIN stock AS s
ON s.isbn = e.isbn
GROUP BY name;
